# NPMRDS Probe Counts

Author: Mark Egge, Data Scientist at [High Street](http://www.highstreetconsulting.com)
Contact: [egge@highstreetconsulting.com](mailto:egge@highstreetconsulting.com)
Last Revision: 11 February 2020

## About

This project provides the analytical tools to calculate probe "density" measures from NPMRDS data. It aggregates the number of probe observations by TMC segment by hour of day, day of week, and month of year.

## Requirements

To run the steps below, you will need to have [Docker Desktop](https://www.docker.com/products/docker-desktop) installed on your computer. Docker Desktop is free, easy to install, and runs on Windows, macOS, and Linux.

## Instructions

You will need to use a command terminal for the steps below.

Download the Source Repository

Spin up the analytical tool:

```bash
git clone https://meggehsc@bitbucket.org/high-street/npmrds_probe_counts.git
cd npmrds_probe_counts
docker-compose up
```

(In the above, `docker-compose up` created a Docker storage volume and then spins up a PostgreSQL database instance connected to that storage volume and a Jupyter notebook instance prepared with the necessary libraries and extensions.)

1. Open your browser to the Jupyter Notebook instance [http://localhost:8888](http://localhost:8888)
2. Enter the password `npmrds`
3. Open Work folder and run the "probe_counts.ipynb" notebook. Follow the instructions in the notebook to finish the data processing steps.

When you are finished, terminate the Docker containers with `docker-compose down`