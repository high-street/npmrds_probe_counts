FROM jupyter/scipy-notebook

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

USER root

RUN apt-get update; \
    apt-get -y install postgis

USER $NB_USER

ENTRYPOINT ["start-notebook.sh"]
CMD ["--NotebookApp.password='sha1:de052db2ed95:aca01b28eaf013dc76af4a79a727b0b604f80ceb'"]